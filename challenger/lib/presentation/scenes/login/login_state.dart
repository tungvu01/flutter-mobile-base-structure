import 'package:challenger/presentation/base/base_state.dart';

class InLogingIn extends BaseState {}

class LoginSuccessState extends BaseState {}

class LoginFailureState extends BaseState {}
