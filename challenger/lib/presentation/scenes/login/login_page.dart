import 'package:flutter/material.dart';
import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_page.dart';
import 'package:challenger/presentation/widgets/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_bloc.dart';
import 'login_event.dart';
import 'login_router.dart';

class LoginPage extends BasePage {
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends BasePageState<LoginBloc, LoginPage, LoginRouter> {
  LoginBloc _bloc;

  @override
  Widget buildBody(BuildContext context, BaseBloc bloc) {
    _bloc = bloc;
    return BlocBuilder<LoginBloc, BaseState>(
      cubit: bloc,
      builder: (context, state) {
        return Center(
          child: Text(
            AppLocalizations.of(context).appName,
            style: getTextStyle(color: Colors.red, fontSize: 20),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }
}
