import 'package:challenger/presentation/base/base_state.dart';
import 'package:flutter/material.dart';
import 'package:challenger/presentation/base/base_router.dart';

class LoginRouter with BaseRouter {
  @override
  onNavigate({BuildContext context, BaseState state}) {}
}
